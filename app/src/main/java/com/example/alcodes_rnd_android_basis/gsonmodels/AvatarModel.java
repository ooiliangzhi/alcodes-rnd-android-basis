package com.example.alcodes_rnd_android_basis.gsonmodels;

public class AvatarModel {
    public Long id;
    public String email;
    public String first_name;
    public String last_name;
    public String avatar;
}
