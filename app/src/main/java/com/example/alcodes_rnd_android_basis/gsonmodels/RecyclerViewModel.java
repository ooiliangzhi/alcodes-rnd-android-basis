package com.example.alcodes_rnd_android_basis.gsonmodels;

import java.util.List;

public class RecyclerViewModel {
    public String page;
    public String per_page;
    public String total;
    public String total_pages;
    public List<AvatarModel> data;
}
