package com.example.alcodes_rnd_android_basis.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.alcodes_rnd_android_basis.Adapter.RecyclerViewAdapter;
import com.example.alcodes_rnd_android_basis.BuildConfig;
import com.example.alcodes_rnd_android_basis.R;
import com.example.alcodes_rnd_android_basis.gsonmodels.AvatarModel;
import com.example.alcodes_rnd_android_basis.gsonmodels.RecyclerViewModel;
import com.example.alcodes_rnd_android_basis.utils.NetworkHelper;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

public class RecyclerViewFragment extends Fragment implements RecyclerViewAdapter.Callbacks {

    public static final String TAG = RecyclerViewFragment.class.getSimpleName();

    @BindView(R.id.avatars_recyclerview)
    protected RecyclerView mRecyclerView;

    private Unbinder mUnbinder;
    private RecyclerViewAdapter mAdapter;
    private List<RecyclerViewAdapter.DataHolder> dataHolders = new ArrayList<>();

    @BindView(R.id.no_data_view)
    protected TextView NoDataView;

    private ProgressDialog progressDialog;

    public RecyclerViewFragment(){
    }

    public static RecyclerViewFragment newInstance() { return new RecyclerViewFragment(); }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();

        progressDialog = new ProgressDialog(getActivity());

        if (!isConnected()) {
            Toast.makeText(getActivity(), "No Internet connection", Toast.LENGTH_LONG).show();
            if(dataHolders.isEmpty()){
                NoDataView.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
            else{
                mRecyclerView.setVisibility(View.VISIBLE);
                NoDataView.setVisibility(View.GONE);
            }
        }
        else{
            if(savedInstanceState != null){
                dataHolders = savedInstanceState.getParcelableArrayList("dataHolders");

                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();
            }else {
                callUserAPI();
            }
        }
    }

    @Override
    public void onDestroy() {
        if (mUnbinder !=null){
            mUnbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onListItemClicked(RecyclerViewAdapter.DataHolder data) {
        Toast.makeText(getActivity(),  "Name: " + data.lastName + " " + data.firstName, Toast.LENGTH_SHORT).show();
    }

    private boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();

    }

    private void initView(){
        mAdapter = new RecyclerViewAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void callUserAPI(){
        if (!progressDialog.isShowing())
            progressDialog.setMessage("Loading...");
        progressDialog.show();

        String url = BuildConfig.BASE_API_URL + "users?page=1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Thread.sleep(1000);
                }catch (Exception e){
                    e.printStackTrace();
                }
                RecyclerViewModel responseModel = new GsonBuilder().create().fromJson(response, RecyclerViewModel.class);
                for (AvatarModel avatarModel : responseModel.data) {
                    RecyclerViewAdapter.DataHolder dataHolder = new RecyclerViewAdapter.DataHolder();
                    dataHolder.id = avatarModel.id;
                    dataHolder.firstName = avatarModel.first_name;
                    dataHolder.lastName = avatarModel.last_name;
                    dataHolder.email = avatarModel.email;
                    dataHolder.image = avatarModel.avatar;
                    dataHolders.add(dataHolder);
                }

                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();

                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Timber.e("Error: " + error.toString());
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("dataHolders", (ArrayList<? extends Parcelable>) dataHolders);
    }
}
