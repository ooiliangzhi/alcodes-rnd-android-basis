package com.example.alcodes_rnd_android_basis.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.alcodes_rnd_android_basis.activities.RecyclerViewActivity;
import com.example.alcodes_rnd_android_basis.activities.TwoWayDataBindingActivity;
import com.google.android.material.button.MaterialButton;

import com.example.alcodes_rnd_android_basis.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainFragment extends Fragment {
    public static final String TAG = MainFragment.class.getSimpleName();

    @BindView(R.id.textview_count_button_clicked)
    protected TextView mTextviewCountButtonClicked;

    @BindView(R.id.button_click_test)
    protected MaterialButton mButtonClickTest;

    @BindView(R.id.button_disable_me_for_5_secs)
    protected MaterialButton mButtonDisableMeFor5Sec;

    @BindView(R.id.button_data_binding)
    protected MaterialButton mButtonDataBinding;

    @BindView(R.id.button_recycler_view)
    protected MaterialButton mButtonRecyclerView;

    private Unbinder mUnbinder;

    private int count = 0;

    private CountDownTimer countDownTimer;

    private long timeLeftInMilliseconds = 6000; // 6 seconds

    private boolean timerRunning = false;

    private static boolean pageChangeTimerRunning = false;

    private long endTime;

    public MainFragment(){
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null) {
            count = savedInstanceState.getInt("count");
            if (count != 0){
                mTextviewCountButtonClicked.setText("Button Clicked: "+count);
            }

            timerRunning = savedInstanceState.getBoolean("timerRunning");
            timeLeftInMilliseconds = savedInstanceState.getLong("timeLeftInMilliseconds");
            endTime = savedInstanceState.getLong("endTime");
            if(timerRunning){
                timeLeftInMilliseconds = endTime - System.currentTimeMillis();
                disableButton();
            }
        }
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onPause(){
        super.onPause();
        if (timerRunning)
            pageChangeTimerRunning = true;
    }

    @Override
    public void onResume(){
        super.onResume();
        if(pageChangeTimerRunning){
            countDownTimer.cancel();
            timeLeftInMilliseconds = endTime - System.currentTimeMillis();
            disableButton();
            pageChangeTimerRunning = false;
        }
    }

    @OnClick(R.id.button_click_test)
    protected void countButtonClick(){
        count++;
        mTextviewCountButtonClicked.setText("Button Clicked: "+count);
    }

    @OnClick(R.id.button_disable_me_for_5_secs)
    protected void disableButton(){
        timerRunning = true;
        endTime = System.currentTimeMillis() + timeLeftInMilliseconds; //To make the count down more accurate
        countDownTimer = new CountDownTimer(timeLeftInMilliseconds, 1000) {
            @Override
            public void onTick(long l) {
                timeLeftInMilliseconds = l;
                mButtonDisableMeFor5Sec.setEnabled(false);
                mButtonDisableMeFor5Sec.setText("Disabled For ("+timeLeftInMilliseconds/1000+"Sec)");
            }

            @Override
            public void onFinish() {
                mButtonDisableMeFor5Sec.setEnabled(true);
                mButtonDisableMeFor5Sec.setText("Disable Me For 5 Sec");
                timeLeftInMilliseconds=6000;
                timerRunning = false;
            }
        }.start();
    }

    @OnClick(R.id.button_data_binding)
    protected void dataBinding(){
        if (timerRunning)
            pageChangeTimerRunning = true;
        startActivity(new Intent(getActivity(), TwoWayDataBindingActivity.class));
    }

    @OnClick(R.id.button_recycler_view)
    protected void displayRecyclerView(){
        if (timerRunning)
            pageChangeTimerRunning = true;
        startActivity(new Intent(getActivity(), RecyclerViewActivity.class));
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count", count);
        if(timerRunning) {
            countDownTimer.cancel();
        }
        outState.putLong("timeLeftInMilliseconds", timeLeftInMilliseconds);
        outState.putLong("endTime", endTime);
        outState.putBoolean("timerRunning", timerRunning);
    }
}
