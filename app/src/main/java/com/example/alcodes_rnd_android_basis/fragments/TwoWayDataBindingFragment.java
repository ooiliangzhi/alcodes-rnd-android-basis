package com.example.alcodes_rnd_android_basis.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.alcodes_rnd_android_basis.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class TwoWayDataBindingFragment extends Fragment {
    public static final String TAG = TwoWayDataBindingFragment.class.getSimpleName();

    @BindView(R.id.edittext_amount1)
    protected EditText mEditTextAmount1;

    @BindView((R.id.edittext_amount2))
    protected EditText mEditTextAmount2;

    @BindView(R.id.edittext_amount3)
    protected EditText mEditTextAmount3;

    @BindView(R.id.textview_total_sum)
    protected TextView mTextViewTotalSum;

    private Unbinder mUnbinder;

    public TwoWayDataBindingFragment(){
    }

    public static TwoWayDataBindingFragment newInstance() { return new TwoWayDataBindingFragment(); }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_2_way_binding, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        getAmount();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void getAmount(){
        mEditTextAmount1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int total = addNumber();
                mTextViewTotalSum.setText("Total: "+total);
                mTextViewTotalSum.setTextColor(Color.parseColor(setColor(total)));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mEditTextAmount2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int total = addNumber();
                mTextViewTotalSum.setText("Total: "+total);
                mTextViewTotalSum.setTextColor(Color.parseColor(setColor(total)));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mEditTextAmount3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int total = addNumber();
                mTextViewTotalSum.setText("Total: "+total);
                mTextViewTotalSum.setTextColor(Color.parseColor(setColor(total)));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public int addNumber(){
        int number1;
        int number2;
        int number3;
        if(mEditTextAmount1.getText().toString() != "" && mEditTextAmount1.getText().length() > 0) {
            number1 = Integer.parseInt(mEditTextAmount1.getText().toString());
        } else {
            number1 = 0;
        }
        if(mEditTextAmount2.getText().toString() != "" && mEditTextAmount2.getText().length() > 0) {
            number2 = Integer.parseInt(mEditTextAmount2.getText().toString());
        } else {
            number2 = 0;
        }
        if(mEditTextAmount3.getText().toString() != "" && mEditTextAmount3.getText().length() > 0) {
            number3 = Integer.parseInt(mEditTextAmount3.getText().toString());
        } else {
            number3 = 0;
        }

        return (number1 + number2 + number3);
    }

    public String setColor(int total){
        if (total<=500){
            return "#239B56";
        }else if (total<=1000){
            return "#F7DC6F";
        }else{
            return "#E74C3C";
        }
    }


    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.button_reset)
    protected void reset(){
        mEditTextAmount1.getText().clear();
        mEditTextAmount2.getText().clear();
        mEditTextAmount3.getText().clear();
    }










}
