package com.example.alcodes_rnd_android_basis.Adapter;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.alcodes_rnd_android_basis.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder implements Parcelable{
        public Long id;
        public String image;
        public String firstName;
        public String lastName;
        public String email;

        public DataHolder(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readLong();
            }
            image = in.readString();
            firstName = in.readString();
            lastName = in.readString();
            email = in.readString();
        }

        public static final Creator<DataHolder> CREATOR = new Creator<DataHolder>() {
            @Override
            public DataHolder createFromParcel(Parcel in) {
                return new DataHolder(in);
            }

            @Override
            public DataHolder[] newArray(int size) {
                return new DataHolder[size];
            }
        };

        public DataHolder() {
            this.id = id;
            this.image = image;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            if (id == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeLong(id);
            }
            parcel.writeString(image);
            parcel.writeString(firstName);
            parcel.writeString(lastName);
            parcel.writeString(email);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_recyclerview_root)
        public LinearLayout root;

        @BindView(R.id.imageview_avatars)
        public ImageView imageView;

        @BindView(R.id.textview_name)
        public TextView name;

        @BindView(R.id.textview_email)
        public TextView email;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            //resetViews();

            if (data != null) {
                Glide.with(root)
                        .load(data.image)
                        .into(imageView);
                name.setText(data.lastName + " " + data.firstName);
                email.setText(data.email);

                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });
                }
            }
        }
    }
    public interface Callbacks {
        void onListItemClicked(DataHolder data);
    }

}
