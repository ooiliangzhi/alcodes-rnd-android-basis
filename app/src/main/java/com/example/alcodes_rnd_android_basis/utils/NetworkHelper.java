package com.example.alcodes_rnd_android_basis.utils;

import android.content.Context;

import androidx.core.content.ContextCompat;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;

public class NetworkHelper {
    private static RequestQueue mRequestQueueInstance;

    public static RequestQueue getRequestQueueInstance(Context context) {
        if (mRequestQueueInstance == null) {
            synchronized (NetworkHelper.class) {
                if (mRequestQueueInstance == null) {
                    Cache cache = new DiskBasedCache(ContextCompat.getCodeCacheDir(context), 1024 * 10240);
                    Network network = new BasicNetwork(new HurlStack());

                    mRequestQueueInstance = new RequestQueue(cache, network);
                    mRequestQueueInstance.start();
                }
            }
        }

        return mRequestQueueInstance;
    }
}
