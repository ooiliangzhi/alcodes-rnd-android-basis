package com.example.alcodes_rnd_android_basis.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.alcodes_rnd_android_basis.R;
import com.example.alcodes_rnd_android_basis.fragments.RecyclerViewFragment;

import butterknife.ButterKnife;

public class RecyclerViewActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recycler_view);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(RecyclerViewFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, RecyclerViewFragment.newInstance(), RecyclerViewFragment.TAG)
                    .commit();
        }
    }
}
