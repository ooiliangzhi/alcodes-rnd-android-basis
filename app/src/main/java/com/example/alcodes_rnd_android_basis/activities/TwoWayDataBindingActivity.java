package com.example.alcodes_rnd_android_basis.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.alcodes_rnd_android_basis.R;
import com.example.alcodes_rnd_android_basis.fragments.MainFragment;
import com.example.alcodes_rnd_android_basis.fragments.TwoWayDataBindingFragment;

import butterknife.ButterKnife;

public class TwoWayDataBindingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_2_way_data_binding);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(TwoWayDataBindingFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, TwoWayDataBindingFragment.newInstance(), TwoWayDataBindingFragment.TAG)
                    .commit();
        }
    }
}
